# blacklab-api

Blacklab connector based on Publisher 7

# DEPRECATED

The code in the repository is no longer in use.  The Early Print Library now interacts
with the REST API of BlackLab directly without the need for an intermediate layer in
eXist.

## Main configuration points

* package.json 
    - set the components version in dependencies: `@teipublisher/pb-components`
* modules/config.xqm
    - components version in `$config:webcomponents`
    - components cdn in `$config:webcomponents-cdn`
    - Blacklab server to query in `$config:blacklab`
* modules/custom-api.json
    - `/api/blacklab/search` path: input for pb-kwic-results (paginated list of documents with matches)
    - `/api/blacklab/doc` path: input for document view (list of matches within a single document)
* modules/app.xql
    - actual functions to support Blacklab endpoints, starting with 
        * `app:blacklab` for `/api/blacklab/search` endpoint
        * `app:blacklab-document` for `/api/blacklab/doc` endpoint

## Build

Just call `ant`. The resulting xar package can be found in `build/blacklab-api-x.x.x.xar