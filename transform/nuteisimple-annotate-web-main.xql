import module namespace m='http://www.tei-c.org/pm/models/nuteisimple-annotate/web' at '/db/apps/blacklab-api/transform/nuteisimple-annotate-web.xql';

declare variable $xml external;

declare variable $parameters external;

let $options := map {
    "styles": ["transform/nuteisimple-annotate.css"],
    "collection": "/db/apps/blacklab-api/transform",
    "parameters": if (exists($parameters)) then $parameters else map {}
}
return m:transform($options, $xml)