xquery version "3.1";

(: 
 : Module for app-specific template functions
 :
 : Add your own templating functions here, e.g. if you want to extend the template used for showing
 : the browsing view.
 :)
module namespace app="teipublisher.com/app";

import module namespace templates="http://exist-db.org/xquery/templates";
import module namespace config="http://www.tei-c.org/tei-simple/config" at "config.xqm";
import module namespace http = "http://expath.org/ns/http-client";
import module namespace console="http://exist-db.org/xquery/console" at "java:org.exist.console.xquery.ConsoleModule";
import module namespace functx = "http://www.functx.com";

declare namespace tei="http://www.tei-c.org/ns/1.0";

declare
    %templates:wrap
function app:foo($node as node(), $model as map(*)) {
    <p>Dummy templating function.</p>
};

declare function app:blacklab-start($offset) {
    let $start :=
    try {
        xs:integer($offset) - 1
    } catch * {
        0
    }
    return if ($start < 0) then 0 else $start
};

declare function app:blacklab-exist-start($offset) {
    let $start :=
        try {
            xs:integer($offset) + 1
        } catch * {
            1
        }
    return if ($start > 0) then $start else 1
};

declare function app:blacklab($request as map(*)) {
    let $endpoint := $config:blacklab?($request?parameters?endpoint)
    let $pattern := encode-for-uri($request?parameters?pattern)
    let $sort := if ($request?parameters?sort) then '&amp;sort=' || encode-for-uri($request?parameters?sort) else ()
    let $format := $request?parameters?format
    let $start := app:blacklab-start($request?parameters?start)
    let $per-page := $request?parameters?per-page
    let $ep := $request?parameters?endpoint

    let $request := 
        <http:request 
            method="GET" 
            href="{$endpoint}?outputformat={$format}&amp;first={$start}&amp;number={$per-page}&amp;patt={$pattern}{$sort}"/>

    let $url := $request/@href
    let $x := console:log('blacklab url: ' || $url)

    let $response := parse-json(util:base64-decode(http:send-request($request)[2]))
                                
    return 

    switch ($ep)
        case "docs" return app:blacklab-docs($response)
        default return app:blacklab-hits($response)
};

declare function app:blacklab-docs($response) {
    map {
        "docs": $response?summary?numberOfDocs,
        "start": app:blacklab-exist-start($response?summary?searchParam?first),
        "per-page": $response?summary?searchParam?number,
        "documents": app:blacklab-doclist($response)
    }
};

declare function app:blacklab-doclist($response) { 
    array:for-each($response?docs, function($doc) {
            map {
                "doc": $doc?docPid,
                "title": $doc?docInfo?display_title,
                "file": $doc?docInfo?fromInputFile,
                "id": $doc?docInfo?idno,
                "hits": $doc?numberOfHits,
                "matches": app:blacklab-snippets($doc?snippets, $doc?docInfo?idno)
            }
    })
};

declare function app:blacklab-snippets($snippets, $docid) {
    array:for-each($snippets, function($hit) {
            map {
                "match": map {
                    "display": app:merge($hit?match?word, $hit?match?punct),
                    "words": array:for-each($hit?match?id, function($id) {
                        $docid || '-' || $id
                    })
                },
                "left": app:merge($hit?left?word, $hit?left?punct),
                "right": app:merge($hit?right?word, $hit?right?punct),
                "page": array:for-each($hit?match?id, function($id) {
                    functx:substring-before-last($id, '-')
                })
            }
    })
};

declare function app:merge($words, $punct) {  
    string-join (
    for $i in 1 to array:size($words)
        return array:get($words, $i) || array:get($punct, $i)
    )
};

declare function app:merge-wordid($pages, $words) {
    array:for-each-pair($pages, $words, function($page, $word) {
        $page[1] || '-' || $word
    })
};

declare function app:blacklab-hits($response) {
    map {
        "docs": $response?summary?numberOfDocs,
        "hits": $response?summary?numberOfHits,
        "start": app:blacklab-exist-start($response?summary?searchParam?first),
        "per-page": $response?summary?searchParam?number,
        "documents": map:merge(app:blacklab-documents($response))
    }
};

declare function app:blacklab-documents($response) {
    if (exists($response?docInfos))
    then
        for $key in map:keys($response?docInfos)[not(. = 'metadataFieldGroups')]
        return
            map {$key: 
                map {
                    "docPid": $key,
                    "title": $response?docInfos?($key)?display_title,
                    "file": $response?docInfos?($key)?fromInputFile,
                    "id": $response?docInfos?($key)?idno,
                    "hits": app:blacklab-matches($response, $key, $response?docInfos?($key)?idno)
                }
            }
    else
        map {}
};

declare function app:blacklab-trim-pageid($page) {
    if ($page instance of array(*)) then 
        array:for-each($page, function($item) {   
            (: data($item) :)
            data(analyze-string(data($item), "A(\d*)-")//*:non-match)
        })
     else
        $page
};

declare function app:blacklab-matches($response, $key, $docid) {
    let $matches :=
    array:for-each($response?hits, function($hit) {
        if ($hit?docPid = $key) then
            map {
                "match": map {
                    "display": app:merge($hit?match?word, $hit?match?punct),
                    "words": array:for-each($hit?match?id, function($id) {
                        $docid || '-' || $id
                    })
                },
                "left": app:merge($hit?left?word, $hit?left?punct),
                "right": app:merge($hit?right?word, $hit?right?punct),
                "page": array:for-each($hit?match?id, function($id) {
                    functx:substring-before-last($id, '-')
                })
            }
        else 
            ()  
    })

    return 

    (: reiterate in order not to pass in nulls as array items :)
    for $i in 1 to array:size($matches)
        return $matches($i)
};


declare function app:flatten($array) {
    if (array:size($array) = 1) then $array(1) else $array
};

declare function app:blacklab-document($request as map(*)) {
    let $endpoint := $config:blacklab?hits
    let $pattern := encode-for-uri($request?parameters?pattern)
    let $format := $request?parameters?format
    let $doc := $request?parameters?doc
    let $number := $request?parameters?per-document

    let $filter := if ($doc) then encode-for-uri('filename:('|| $doc ||')') else ()
    
    let $request := 
        <http:request 
            method="GET" 
            href="{$endpoint}?outputformat={$format}&amp;filter={$filter}&amp;patt={$pattern}&amp;number={$number}"/>
    
    let $url := $request/@href
    let $x := console:log('blacklab url: ' || $url)
    let $response := parse-json(util:base64-decode(http:send-request($request)[2]))
                                
    return 
        app:blacklab-document-hits($response, $number)
};

declare function app:blacklab-document-hits($response, $number) {
    let $documents := map:merge(app:blacklab-documents($response))
    return
    map {
        "hits": $response?summary?numberOfHits,
        "docs": $response?summary?numberOfDocs,
        "available": $response?summary?numberOfHitsRetrieved,
        "retrieved": $number,
        "documents": $documents
    }
};